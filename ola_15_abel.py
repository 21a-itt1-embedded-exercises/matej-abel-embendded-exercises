import utime 
import machine

led1 = machine.Pin(14,machine.Pin.OUT) 
led2 = machine.Pin(15,machine.Pin.OUT) 
btn1 = machine.Pin(18,machine.Pin.IN, machine.Pin.PULL_UP)
btn2 = machine.Pin(19,machine.Pin.IN, machine.Pin.PULL_UP)
led_onboard = machine.Pin(25,machine.Pin.OUT)

led1.value(0)
led2.value(0)

def blink_1(led_A): 
    led_A.value(1) 
    utime.sleep(0.1)
    led_A.value(0) 

    
def blink_2(led_B): 
    blink_1(led_B) 
    utime.sleep(0.1)
    blink_1(led_B) 
    
while True:                 
    if btn1.value() == 0:    
        blink_1(led1)        
        blink_1(led_onboard) 
    
    if btn2.value() == 0:    
        blink_1(led2)        
        blink_2(led_onboard) 
        
