import utime # importing the utime library for usage in our pico 
import machine# importing the machine library for usage of pico

led1 = machine.Pin(14,machine.Pin.OUT) # setting the particular GPIO output for red LED1 
led2 = machine.Pin(15,machine.Pin.OUT) # setting the particular GPIO output for red LED2 
btn1 = machine.Pin(18,machine.Pin.IN, machine.Pin.PULL_UP)# setting the particular GPIO input for Button 1  
btn2 = machine.Pin(19,machine.Pin.IN, machine.Pin.PULL_UP)# setting the particular GPIO input for Button 2 
led_onboard = machine.Pin(25,machine.Pin.OUT)# setting the particular GPIO pin for buited-in green LED on Pico

led1.value(0)#turning the LED1 off, for having it off when the loop is not happening
led2.value(0)#turning the LED2 off, for having it off when the loop is not happening

def blink_1(led_A): # setting of the first function for usage in our program
    led_A.value(1) # turning on the later be specified LED by letting the current flow through the circuit
    utime.sleep(0.1) # forcing the system to sleep for one millisecont
    led_A.value(0) # turning off the later be specified LED by closing the current flow through the circuit

    
def blink_2(led_B): # setting of the second function for usage in our program
    blink_1(led_B) # turning on the later be specified LED by using the first function
    utime.sleep(0.1) # forcing the system to sleep for one millisecont
    blink_1(led_B) # turning on the later be the specified LED again, which will make specified LED, blinking for the second time in the loop
    
while True:                  # creating an infinite loop 
    if btn1.value() == 0:    # setting a condition for the button being pushed, in which the program will execute('1' would mean that button is not pressed)
        blink_1(led1)        # the first red LED is working by the order of first function 
        blink_1(led_onboard) # the green LED builded-in the Pico is working by the order of first function 
    
    if btn2.value() == 0:    # setting a condition for the button being pushed, in which the program will execute('1' would mean that button is not pressed)
        blink_1(led2)        # the second red LED is working by the order of first function
        blink_2(led_onboard) # the green LED builded-in the Pico is working by the order of second function
        