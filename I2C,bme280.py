from machine import Pin, SoftI2C
import BME280 as bme280
import utime

i2c = SoftI2C(scl=Pin(1),sda=Pin(0), freq=10000)
bme = bme280.BME280(i2c=i2c, address=0x77)

print(bme.values)

(temperature, pressure, humidity) = bme.values
print(temperature)
print(pressure)
print(humidity)

while True:
    bme = bme280.BME280(i2c=i2c, address=0x77)
    (temperature, pressure, humidity) = bme.values
    utime.sleep(2)
    if temperature>str(30):
        print('Turn down the heat!',(temperature))
    elif temperature<str(25):
        print('Did you open the window',(temperature))
    else:
        print('The temperature is perfest!')
    

    